﻿using System;
using System.AddIn;
using addinview = eMite.Framework.Bcl.Adapters.AddInView;
using eob = eMite.Framework.Bcl.Onboarding;
using eMite.Framework.Bcl.Onboarding.DTO;
using System.Reflection;
using System.Threading;

namespace eMite.AmazonKinesisRT.Adapter.BM
{
    [AddIn("AmazonKinesisRT")]
    public partial class BM_Plugin : addinview.BASE.BM.BM_AdapterBase
    {
        #region "Overrides"

        /// <summary>
        /// The Main() method is the key entry point for the business logic that the adapter provides to the adapter framework. 
        /// This method is called by the adapter framework to process and provide data for onboarding into the eMite database or indexes.
        /// </summary>
        /// <returns>DTO_Transfer object for onboarding into eMite</returns>

        public override DTO_Transfer Main()
        {
            LogVersion();

            eob.DTO.DTO_Transfer DataTransferObject = null;

            //Call the Pure Cloud RT adapter controller that contains all the execution logic.
            using (Api.BM.BM_AdapterController Controller = new Api.BM.BM_AdapterController(config))
            {
                Controller.Process();
            }

            Log.Info("Adapter exited!");

            return DataTransferObject;

        }

        #endregion

        #region "Private Methods"

        #endregion

        #region "Public Methods"
        /// <summary>
        /// Get the current version of the program
        /// </summary>
        public void LogVersion()
        {
            var Version = Assembly.GetExecutingAssembly().GetName().Version;
            Log.Info("AWS Kinesis - RealTime Adapter  " + Version.Major + "." + Version.Minor + "." + Version.Build + " Started");

        }
        #endregion

    }
}
