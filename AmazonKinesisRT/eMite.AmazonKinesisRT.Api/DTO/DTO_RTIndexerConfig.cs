﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eMite.AmazonKinesisRT.Api.DTO
{
    public class DTO_RTIndexerConfig
    {
        public string Id { get; set; }
        public List<string> DateFields { get; set; }
        public string MessageGroupName { get; set; }
        public string IndexGroup { get; set; }
        public string DataType { get; set; }

        public DTO_RTIndexerConfig(string id, List<string> dateFields, string messageGroupName, string indexGroup, string dataType)
        {
            Id = id;
            DateFields = dateFields;
            MessageGroupName = messageGroupName;
            IndexGroup = indexGroup;
            DataType = dataType;
        }
    }
}
