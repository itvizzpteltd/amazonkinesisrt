﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eMite.AmazonKinesisRT.Api.DTO.Commands
{

    public class DTO_AdapterCommand
    {
        public string CommandType { get; set; }

        public object Data { get; set; }
    }

}
