﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using bcl = eMite.Framework.Bcl;

namespace eMite.AmazonKinesisRT.Api.DTO.AgentEvents
{
    public class DTO_AgentEventsCache : bcl.Base.BM.BM_NoCrudBase
    {
        #region "Properties"
        public List<DTO.AgentEvent.DTO_AgentEvent> AgentEvents { get; set; }
        private readonly object agentLock = new object();
        #endregion

        #region "Constructors"

        public DTO_AgentEventsCache()
        {
            AgentEvents = new List<DTO.AgentEvent.DTO_AgentEvent>();
        }
        #endregion

        #region "Methods"
        public DTO_AgentEventsCache updateCache(AgentEvent.DTO_AgentEvent agentEvent, DTO_AgentEventsCache AgentsEventsCache)
        {
            lock (agentLock)
            {
                try
                {
                    string arn = agentEvent.AgentArn;

                    var result = AgentsEventsCache.AgentEvents.Where(p => p.AgentArn == arn);

                    if (result.Count() != 0)
                    {
                        //Remove the old agent data from cache
                        var itemToRemove = AgentEvents.Single(r => r.AgentArn == arn);
                        AgentsEventsCache.AgentEvents.Remove(itemToRemove);

                        //Add the new agent data to the cache
                        AgentsEventsCache.AgentEvents.Add(agentEvent);
                    }
                    else
                    {
                        //Agent data missing from cache update add the agent data
                        AgentsEventsCache.AgentEvents.Add(agentEvent);
                    }

                }
                catch (Exception ex)
                {
                    Log.Error("Exception occurred in: " + MethodBase.GetCurrentMethod().Name);
                    Log.Error("Exception details: " + ex.StackTrace);
                }
            }
            return AgentsEventsCache;
        }

        #endregion
    }
}
