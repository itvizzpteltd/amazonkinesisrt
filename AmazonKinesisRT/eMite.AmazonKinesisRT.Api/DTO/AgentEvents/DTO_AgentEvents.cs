﻿using Newtonsoft.Json;
using System;

namespace eMite.AmazonKinesisRT.Api.DTO.AgentEvent
{
    public class DTO_AgentEvent
    {
        //[JsonProperty("AWSAccountId")]
        //public string AwsAccountId { get; set; }

        [JsonProperty("AgentARN")]
        public string AgentArn { get; set; }

        [JsonProperty("CurrentAgentSnapshot")]
        public CurrentAgentSnapshot CurrentAgentSnapshot { get; set; }

        //[JsonProperty("EventId")]
        //public string EventId { get; set; }

        [JsonProperty("EventTimestamp")]
        public DateTime EventTimestamp { get; set; }

        [JsonProperty("EventType")]
        public string EventType { get; set; }

        //[JsonProperty("InstanceARN")]
        //public string InstanceArn { get; set; }

        //[JsonProperty("Version")]
        //public DateTime Version { get; set; }
    }

    public class CurrentAgentSnapshot
    {
        [JsonProperty("AgentStatus")]
        public AgentStatus AgentStatus { get; set; }

        [JsonProperty("Configuration")]
        public Configuration Configuration { get; set; }

        [JsonProperty("Contacts")]
        public Contact[] Contacts { get; set; }
    }

    public class AgentStatus
    {
        [JsonProperty("ARN")]
        public string Arn { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("StartTimestamp")]
        public DateTime StartTimestamp { get; set; }
    }

    public class Configuration
    {
        //[JsonProperty("AgentHierarchyGroups")]
        //public object AgentHierarchyGroups { get; set; }

        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("RoutingProfile")]
        public RoutingProfile RoutingProfile { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }
    }

    public class RoutingProfile
    {
        //[JsonProperty("ARN")]
        //public string Arn { get; set; }

        [JsonProperty("DefaultOutboundQueue")]
        public Queue DefaultOutboundQueue { get; set; }

        [JsonProperty("InboundQueues")]
        public Queue[] InboundQueues { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }
    }

    public class Queue
    {
        [JsonProperty("ARN")]
        public string Arn { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }
    }

    public class Contact
    {
        [JsonProperty("Channel")]
        public string Channel { get; set; }

        [JsonProperty("ConnectedToAgentTimestamp")]
        public DateTime? ConnectedToAgentTimestamp { get; set; }

        [JsonProperty("ContactId")]
        public string ContactId { get; set; }

        [JsonProperty("InitialContactId")]
        public object InitialContactId { get; set; }

        [JsonProperty("InitiationMethod")]
        public string InitiationMethod { get; set; }

        [JsonProperty("Queue")]
        public Queue Queue { get; set; }

        [JsonProperty("QueueTimestamp")]
        public object QueueTimestamp { get; set; }

        [JsonProperty("State")]
        public string State { get; set; }

        [JsonProperty("StateStartTimestamp")]
        public DateTime StateStartTimestamp { get; set; }
    }
}
