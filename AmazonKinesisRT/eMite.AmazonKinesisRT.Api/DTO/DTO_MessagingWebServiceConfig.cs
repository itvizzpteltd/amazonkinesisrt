﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eMite.AmazonKinesisRT.Api.DTO
{
    public class DTO_MessagingWebServiceConfig
    {
        #region "Properties"

        public string Publisher { get; set; }
        public string MessageGroup { get; set; }
        public string MessageGroupInstance { get; set; }

        #endregion

        #region "Constructors"
        public DTO_MessagingWebServiceConfig()
        {
            Publisher = Misc.Utilites.InstanceName;
            MessageGroup = "Dashboard";
            MessageGroupInstance = "LiveData";
        }
        #endregion
    }
}
