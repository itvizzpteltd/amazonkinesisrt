﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eMite.AmazonKinesisRT.Api.Misc
{
    public class Utilites
    {
        #region "Properties"
        public static string InstanceName { get; set; }
        public static Helpers.BM_AdapterData BmAdapterData { get; set; }
        #endregion

        #region "Methods"

        /// <summary>
        /// Initialize is called once during the start of the adapter to initialize shared data and business modules 
        /// </summary>
        /// <param name="config"></param>
        public static void Initialize(DTO.DTO_Config config)
        {
            if (BmAdapterData == null)
                BmAdapterData = new Helpers.BM_AdapterData(config);
        }




        #endregion
    }
}
