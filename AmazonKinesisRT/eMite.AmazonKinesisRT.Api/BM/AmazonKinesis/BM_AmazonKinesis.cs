﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eMite.Framework.Bcl.DTO;
using bcl = eMite.Framework.Bcl;

namespace eMite.AmazonKinesisRT.Api.BM
{
    public class BM_AmazonKinesis : BASE.BM.BM_AmazonKinesisRTBase, IDisposable
    {
        #region "Properties"
        #endregion

        #region "Constructors"
        public BM_AmazonKinesis(DTO.DTO_Config config)
        {
            Dac = new DAC.DAC_AmazonKinesisRT(config);
        }

        #endregion

        #region "Overrides"
        public override bcl.DTO.DTO_Result Start()
        {
            return Dac.Start();
        }

        public override void Dispose()
        {
            Dac.Dispose();
        }

        #endregion

        #region "Public Methods"
        #endregion
    }
}
