﻿using System;
using bcl = eMite.Framework.Bcl;

namespace eMite.AmazonKinesisRT.Api.BM
{
    public class BM_AdapterController : bcl.Base.BM.BM_NoCrudBase, IDisposable
    {
        #region "Properties"        
        private DTO.DTO_Config Config { get; set; }
        private BM_AmazonKinesis bmAmazonKinesis;
        #endregion

        #region "Constructors" 
        /// <summary>
        /// Constructor accepting the adapter configurations object. 
        /// </summary>
        /// <param name="config"></param>
        public BM_AdapterController(DTO.DTO_Config config)
        {
            Config = config;
            Misc.Utilites.Initialize(Config);

            //BM initialization
            bmAmazonKinesis = new BM_AmazonKinesis(config);
        }
        #endregion

        #region "Methods"

        public void Process()
        {
            bcl.DTO.DTO_Result ExitResults = new Framework.Bcl.DTO.DTO_Result();
            bmAmazonKinesis.Start();

        }

        public void Dispose()
        {
        }
        #endregion
    }
}
