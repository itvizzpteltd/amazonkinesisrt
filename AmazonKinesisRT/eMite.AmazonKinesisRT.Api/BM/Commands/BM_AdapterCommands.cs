﻿using Newtonsoft.Json.Linq;
using System;
using bcl = eMite.Framework.Bcl;
using msg = eMite.Framework.Bcl.Messaging;

namespace eMite.AmazonKinesisRT.Api.BM.Commands
{
    public class BM_AdapterCommands : bcl.Base.BM.BM_NoCrudBase, IDisposable
    {
        #region "Properties"
        private string Publisher { get; set; }
        private string MessageGroup { get; set; }
        private string Instance { get; set; }
        private msg.BM.BM_Message BmMessage { get; set; }
        protected DTO.DTO_MessagingWebServiceConfig MessagingWebServceConfig { get; set; }

        public delegate void AdapterCommandHandler(DTO.Commands.DTO_AdapterCommand adapterCommand, string publisher);

        public event AdapterCommandHandler AdapterCommandHandlerEvent;
        #endregion

        #region "Constructors"
        public BM_AdapterCommands()
        {

        }
        #endregion

        #region "Methods"
        public void Dispose()
        {
            if (BmMessage != null)
            {
                Log.Trace("Disposing BM_AdapterCommands");
                BmMessage.Dispose();
            }
        }

        public void ConnectToMessagingWebService()
        {
            Log.Info("Connecting to eMite Messaging Web Service");

            try
            {

                MessagingWebServceConfig = new DTO.DTO_MessagingWebServiceConfig();

                Publisher = MessagingWebServceConfig.Publisher;
                MessageGroup = "Adapter";
                Instance = "Commands";

                msg.DTO.DTO_MessagingWebServiceConnection connection = new msg.DTO.DTO_MessagingWebServiceConnection(Publisher,
                                                                            MessageGroup,
                                                                            Instance,
                                                                            queueType: eMite.Framework.Bcl.Messaging.MISC.Utilities.QueueType.INMEM,
                                                                            hubConnectionType: eMite.Framework.Bcl.Messaging.ENUM.HubConnectionType.FullDuplex, maxConnections: 100);


                Log.Info("Messaging Url: " + connection.MessagingWebServiceUrl);

                BmMessage = new eMite.Framework.Bcl.Messaging.BM.BM_Message(connection);

                BmMessage.Message += BmMessage_Message;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                if (ex.InnerException != null)
                    Log.Error("Inner Exception: " + ex.InnerException.Message);
                Log.Error(ex.StackTrace);
            }
        }

        private void BmMessage_Message(msg.DTO.DTO_Message message)
        {

            JObject jObject = (JObject)message.Message;
            DTO.Commands.DTO_AdapterCommand AdapterCommand = jObject.ToObject<DTO.Commands.DTO_AdapterCommand>();

            RaiseAdapterCommandEvent(AdapterCommand, message.Publisher);

        }

        protected void RaiseAdapterCommandEvent(DTO.Commands.DTO_AdapterCommand adapterCommand, string publisher)
        {
            try
            {
                if (adapterCommand != null)
                {
                    AdapterCommandHandlerEvent(adapterCommand, publisher);
                }
                else
                    Log.Info("Null message received - No event raised");
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                Log.Error(ex.StackTrace);
            }
        }
        #endregion
    }
}
