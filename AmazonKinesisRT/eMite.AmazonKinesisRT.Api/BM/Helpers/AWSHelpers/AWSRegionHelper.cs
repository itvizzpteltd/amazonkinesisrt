﻿using Amazon;
using System;
using bcl = eMite.Framework.Bcl;

namespace eMite.AmazonKinesisRT.Api.BM.Helpers.AWSHelpers
{
    public class AWSRegionHelper : bcl.Base.BM.BM_NoCrudBase
    {

        /// <summary>
        /// Returns AWS region object based on user input form config file
        /// </summary>
        /// <returns></returns>
        public RegionEndpoint GetAWSRegion(string awsRegion)
        {
            RegionEndpoint regionEndpoint = null;

            try
            {
                //Validation
                if (string.IsNullOrEmpty(awsRegion))
                {
                    Log.Info("Please add the valid AWSRegion in the adapter configuration");
                }
                else
                {
                    Log.Info("Set the default AWS region for adapter as: " + awsRegion);

                    #region<Switch case>

                    switch (awsRegion.ToLower())
                    {
                        case "apnortheast1":
                            regionEndpoint = RegionEndpoint.APNortheast1;
                            break;
                        case "apnortheast2":
                            regionEndpoint = RegionEndpoint.APNortheast2;
                            break;
                        //case "apsouth1":
                        //    regionEndpoint = RegionEndpoint.APSouth1;
                        //    break;
                        case "apsoutheast1":
                            regionEndpoint = RegionEndpoint.APSoutheast1;
                            break;
                        case "apsoutheast2":
                            regionEndpoint = RegionEndpoint.APSoutheast2;
                            break;
                        //case "cacentral1":
                        //    regionEndpoint = RegionEndpoint.CACentral1;
                        //    break;
                        case "cnnorth1":
                            regionEndpoint = RegionEndpoint.CNNorth1;
                            break;
                        case "eucentral1":
                            regionEndpoint = RegionEndpoint.EUCentral1;
                            break;
                        case "euwest1":
                            regionEndpoint = RegionEndpoint.EUWest1;
                            break;
                        //case "euwest2":
                        //    regionEndpoint = RegionEndpoint.EUWest2;
                        //    break;
                        case "saeast1":
                            regionEndpoint = RegionEndpoint.SAEast1;
                            break;
                        case "useast1":
                            regionEndpoint = RegionEndpoint.USEast1;
                            break;
                        //case "useast2":
                        //    regionEndpoint = RegionEndpoint.USEast2;
                        //    break;
                        case "usgovcloudwest1":
                            regionEndpoint = RegionEndpoint.USGovCloudWest1;
                            break;
                        case "uswest1":
                            regionEndpoint = RegionEndpoint.USWest1;
                            break;
                        case "uswest2":
                            regionEndpoint = RegionEndpoint.USWest2;
                            break;
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception occurred: " + ex);
            }

            return regionEndpoint;
        }

    }
}

