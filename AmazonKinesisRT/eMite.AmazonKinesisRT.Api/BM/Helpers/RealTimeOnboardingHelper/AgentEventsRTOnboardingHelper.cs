﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eMite.Framework.Bcl.Messaging.DTO;
using bcl = eMite.Framework.Bcl;
using eMite.AmazonKinesisRT.Api.DTO;

namespace eMite.AmazonKinesisRT.Api.BM.Helpers
{
    public class AgentEventsRTOnboardingHelper : bcl.Adapters.Common.BASE.DAL.DAL_OnboardingBase
    {
        public AgentEventsRTOnboardingHelper(DTO_MessageConfig messageConfig) : base(messageConfig)
        {
        }

        #region<properties>        
        public string Id { get; set; }
        public List<string> DateFields { get; set; }
        public string MessageGroupName { get; set; }
        public string IndexGroup { get; set; }
        public string DataType { get; set; }
        #endregion

        public void OnboardBatch(dynamic message, int batchNumber, int batchCount, string targetClient)
        {
            //send the message to the messaging web service.
            SendMessageBatch(message, batchNumber, batchCount, targetClient);
        }


    }
}
