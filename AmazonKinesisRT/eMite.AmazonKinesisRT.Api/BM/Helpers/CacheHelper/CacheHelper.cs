﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace eMite.AmazonKinesisRT.Api.BM.Helpers.CacheHelper
{
    public class CacheHelper
    {
        public void SaveData(string name, object cacheObj)
        {
            try
            {
                CacheItemPolicy cachePolicy = new CacheItemPolicy();
                cachePolicy.AbsoluteExpiration = DateTimeOffset.MaxValue;

                ObjectCache cache = MemoryCache.Default;

                cache.Set(name, cacheObj, cachePolicy);

            }
            catch (Exception ex)
            {

            }
        }

        public object GetData(string cacheName)
        {
            try
            {
                ObjectCache cache = MemoryCache.Default;

                object cacheObj = cache[Constant.AGENT_STATUS] as object;
                return cache.Get(Constant.AGENT_STATUS);
            }
            catch (Exception ex)
            {

                return null;
            }

        }
    }
}
