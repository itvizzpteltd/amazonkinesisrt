﻿using Amazon.Kinesis;
using Amazon.Kinesis.Model;
using eMite.AmazonKinesisRT.Api.BM.Helpers;
using eMite.AmazonKinesisRT.Api.BM.Helpers.AWSHelpers;
using System;
using System.Collections.Generic;
using System.Text;
using bcl = eMite.Framework.Bcl;
using Newtonsoft.Json;
using System.Dynamic;
using System.Reflection;
using eMite.AmazonKinesisRT.Api.Helpers;
using eMite.AmazonKinesisRT.Api.DTO;
using Newtonsoft.Json.Linq;
using System.Runtime.Caching;
using eMite.AmazonKinesisRT.Api.BM.Helpers.CacheHelper;
using System.Linq;
using eMite.Framework.Bcl.Messaging.DTO;
using msg = eMite.Framework.Bcl.Messaging;
using System.Threading;

namespace eMite.AmazonKinesisRT.Api.DAL
{
    public class DAL_AmazonKinesis : BASE.DAL.DAL_AmazonKinesisRT
    {

        #region "Properties"
        private DTO.DTO_Config UserConfig { get; set; }
        private BM.Commands.BM_AdapterCommands BmAdapterCommands { get; set; }
        CacheHelper cacheHelper = new CacheHelper();
        private static DTO.AgentEvents.DTO_AgentEventsCache AgentsEventsCache = new DTO.AgentEvents.DTO_AgentEventsCache();
        DTO.DTO_MessagingWebServiceConfig MessagingWebServceConfig = new DTO_MessagingWebServiceConfig();
        protected msg.BM.BM_Message BmMessages { get; set; }


        #endregion

        #region "Constructors"
        public DAL_AmazonKinesis(DTO.DTO_Config config)
        {
            UserConfig = config;

            Misc.Utilites.Initialize(config);


        }

        #endregion

        #region "Overrides"
        public override bcl.DTO.DTO_Result Start()
        {
            bcl.DTO.DTO_Result Results = new Framework.Bcl.DTO.DTO_Result();

            ReadFromKinesis();

            return Results;
        }

        public override void Dispose()
        {

        }


        #endregion

        #region "Public Methods"
        #endregion

        #region "Private Methods"

        /// <summary>
        /// This method is the entry point for the kinesis adapter
        /// </summary>
        private void ReadFromKinesis()
        {
            //BM_AdapterData adapterData = new BM_AdapterData(UserConfig);
            string iterator = string.Empty;
            BmAdapterCommands = new BM.Commands.BM_AdapterCommands();
            BmAdapterCommands.ConnectToMessagingWebService();
            BmAdapterCommands.AdapterCommandHandlerEvent += BmAdapterCommands_AdapterCommandHandlerEvent;


            try
            {
                AWSRegionHelper regionHelper = new AWSRegionHelper();

                //create config that points to Kinesis region
                AmazonKinesisConfig config = new AmazonKinesisConfig();
                config.RegionEndpoint = regionHelper.GetAWSRegion(UserConfig.AWSRegion);

                //aws credentials
                var awsCredentials = new Amazon.Runtime.BasicAWSCredentials(UserConfig.AWSAccessKeyID, UserConfig.AWSSecretAccessKey);

                //create new client object
                AmazonKinesisClient client = new AmazonKinesisClient(awsCredentials, config.RegionEndpoint);

                Log.Debug("AWS AccessKeyID: " + UserConfig.AWSAccessKeyID);
                Log.Debug("AWS SecretAccessKey: " + UserConfig.AWSSecretAccessKey);

                //Step #1 - describe stream to find out the shards it contains
                DescribeStreamRequest describeRequest = new DescribeStreamRequest();
                describeRequest.StreamName = UserConfig.StreamName;

                DescribeStreamResponse describeResponse = client.DescribeStream(describeRequest);
                List<Shard> shards = describeResponse.StreamDescription.Shards;


                //grab the only shard ID in this stream
                string primaryShardId = shards[0].ShardId;

                //Step #2 - get iterator for this shard
                GetShardIteratorRequest iteratorRequest = new GetShardIteratorRequest();
                iteratorRequest.StreamName = UserConfig.StreamName;
                iteratorRequest.ShardId = primaryShardId;
                iteratorRequest.ShardIteratorType = ShardIteratorType.TRIM_HORIZON;

                GetShardIteratorResponse iteratorResponse = client.GetShardIterator(iteratorRequest);
                iterator = iteratorResponse.ShardIterator;

                //Step #3 - get records in this iterator
                do
                {
                    iterator = ProcessKinesisRecords(client, iterator);
                    Thread.Sleep(100);
                }
                while (iterator != string.Empty);
            }
            catch (Exception ex)
            {
                Log.Error("Exception occurred in " + MethodBase.GetCurrentMethod().Name + ex.StackTrace);
                Log.Info("Restarting the application \n");

                Log.Info("Application restarted at: " + DateTime.UtcNow);
                ReadFromKinesis();

            }

        }

        /// <summary>
        /// This methodhandles the adapter commands when user refresh the page
        /// </summary>
        /// <param name="adapterCommand"></param>
        /// <param name="publisher"></param>
        private void BmAdapterCommands_AdapterCommandHandlerEvent(DTO.Commands.DTO_AdapterCommand adapterCommand, string publisher)
        {
            Log.Info("Adapter Command Received (AdapterController): " + bcl.Common.BM.BM_SerializationHelper.Serialize(adapterCommand));

            try
            {
                if (adapterCommand.CommandType.ToLower() == "cacherequest")
                {

                    JObject jObject = (JObject)adapterCommand.Data;

                    DTO.Commands.DTO_CacheRequest CacheRequestCommand = jObject.ToObject<DTO.Commands.DTO_CacheRequest>();

                    if (CacheRequestCommand.FactQuery.ToLower() == UserConfig.FactQueryName.ToLower())
                    {
                        SendDataForOnboarding();
                    }

                }

                if (adapterCommand.CommandType.ToLower() == "staticdata")
                {
                    SendDataForOnboarding();
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message + " | " + ex.StackTrace);
            }
        }

        /// <summary>
        /// This method reads the kinesis records from the strea using the AmazonKinesisCLient and iterator
        /// </summary>
        /// <param name="client"></param>
        /// <param name="iteratorId"></param>
        /// <returns></returns>
        private string ProcessKinesisRecords(AmazonKinesisClient client, string iteratorId)
        {
            try
            {
                //create request
                GetRecordsRequest getRequest = new GetRecordsRequest();
                getRequest.Limit = 100;
                getRequest.ShardIterator = iteratorId;

                //call "get" operation and get everything in this shard range
                GetRecordsResponse getResponse = client.GetRecords(getRequest);
                //get reference to next iterator for this shard
                string nextIterator = getResponse.NextShardIterator != null ? getResponse.NextShardIterator : string.Empty;
                DTO.AgentEvent.DTO_AgentEvent agentEvent = new DTO.AgentEvent.DTO_AgentEvent();

                //retrieve records
                List<Record> records = getResponse.Records;

                foreach (Record r in records)
                {
                    //pull out (JSON) data in this record
                    string agentStatusData = Encoding.UTF8.GetString(r.Data.ToArray());

                    if (!string.IsNullOrEmpty(agentStatusData))
                    {
                        JObject awsKinesisStreamJobj = JObject.Parse(agentStatusData);

                        if (awsKinesisStreamJobj != null)
                        {
                            if (awsKinesisStreamJobj.SelectToken("EventType").ToString().ToLower() != "heart_beat")
                            {
                                agentEvent = JsonConvert.DeserializeObject<DTO.AgentEvent.DTO_AgentEvent>(agentStatusData);

                                if (agentEvent.CurrentAgentSnapshot.Contacts.Length > 0)
                                {
                                    checkAgentState(agentEvent);
                                }


                                Log.Info(agentStatusData);

                                if (agentEvent.CurrentAgentSnapshot != null)
                                {
                                    //UpdateCache
                                    CheckAndUpdateCache(agentEvent);
                                    SendDataForOnboarding();
                                }

                                Log.Debug("Agent event data received: " + agentStatusData);
                            }
                        }
                    }
                }


                return nextIterator;
            }
            catch (Exception ex)
            {
                Log.Error("Exception occurred in " + ex.StackTrace);
                return string.Empty;
            }

        }

        /// <summary>
        /// This method checks and update the local memory
        /// </summary>
        /// <param name="agentEvent"></param>
        private void CheckAndUpdateCache(DTO.AgentEvent.DTO_AgentEvent agentEvent)
        {
            try
            {
                AgentsEventsCache = AgentsEventsCache.updateCache(agentEvent, AgentsEventsCache);
            }
            catch (Exception ex)
            {
                Log.Error("Exception occurred in: " + MethodBase.GetCurrentMethod().Name);
                Log.Error("Exception details: " + ex.StackTrace);
            }

        }

        /// <summary>
        /// This methods sends the data for onboarding
        /// </summary>
        private void SendDataForOnboarding()
        {
            #region<properties>
            int TotalBatches = 0;
            int MessageBatchSize = 35;
            int BatchNo = 0;
            dynamic message = new ExpandoObject();
            DTO_DashboardMessageDynamicList messageList = new DTO_DashboardMessageDynamicList(); bcl.Messaging.DTO.DTO_MessageConfig MessagingConfig = new Framework.Bcl.Messaging.DTO.DTO_MessageConfig("AgentEventsRT", "dashboard", "livedata");
            AgentEventsRTOnboardingHelper RTOnboardingHelper = new AgentEventsRTOnboardingHelper(MessagingConfig);
            #endregion

            messageList.PrimaryKey = "AgentArn";
            messageList.DateColumns = new List<string>() { "StartTimestamp", "EventTimestamp" }.ToArray();
            messageList.FactQuery = UserConfig.FactQueryName;
            messageList.IndexGroup = "Emite";
            messageList.Type = "IndexChanged";
            messageList.Content = new List<dynamic>();

            try
            {
                //create the emite message list
                foreach (var agent in AgentsEventsCache.AgentEvents)
                {
                    message.AgentArn = agent.AgentArn;
                    message.AgentStatus = agent.CurrentAgentSnapshot.AgentStatus.Name;
                    message.StartTimestamp = agent.CurrentAgentSnapshot.AgentStatus.StartTimestamp.ToLocalTime().ToUniversalTime();
                    message.AgentName = agent.CurrentAgentSnapshot.Configuration.FirstName + " " + agent.CurrentAgentSnapshot.Configuration.LastName;
                    message.RoutingProfileName = agent.CurrentAgentSnapshot.Configuration.RoutingProfile.Name;
                    message.DefaultOutboundQueueName = agent.CurrentAgentSnapshot.Configuration.RoutingProfile.DefaultOutboundQueue.Name;
                    message.AgentUserName = agent.CurrentAgentSnapshot.Configuration.Username;
                    message.EventTimestamp = agent.EventTimestamp.ToLocalTime().ToUniversalTime();
                    message.EventType = agent.EventType;

                    //Add the message from stream is the agent is on call else assign 'NA' string
                    if (agent.CurrentAgentSnapshot.Contacts.Length > 0)
                    {
                        message.Channel = agent.CurrentAgentSnapshot.Contacts[0].Channel;
                        message.ConnectedToAgentTimestamp = agent.CurrentAgentSnapshot.Contacts[0].ConnectedToAgentTimestamp;
                        message.ContactId = agent.CurrentAgentSnapshot.Contacts[0].ContactId;
                        message.InitialContactId = agent.CurrentAgentSnapshot.Contacts[0].InitialContactId;
                        message.InitiationMethod = agent.CurrentAgentSnapshot.Contacts[0].InitiationMethod;
                        message.QueueName = agent.CurrentAgentSnapshot.Contacts[0].Queue.Name;
                        message.QueueTimestamp = agent.CurrentAgentSnapshot.Contacts[0].QueueTimestamp;
                        message.State = agent.CurrentAgentSnapshot.Contacts[0].State;
                        message.Channel = agent.CurrentAgentSnapshot.Contacts[0].StateStartTimestamp;
                    }
                    else
                    {
                        message.Channel = "-";
                        message.ConnectedToAgentTimestamp = "-";
                        message.ContactId = "-";
                        message.InitialContactId = "-";
                        message.InitiationMethod = "-";
                        message.InitiationMethod = "-";
                        message.Queue = "-";
                        message.QueueTimestamp = "-";
                        message.State = "-";
                        message.Channel = "-";
                    }

                    //Add the message in the message list object
                    messageList.Content.Add(message);

                    //Reset the message object 
                    message = new ExpandoObject();
                }

                //Message list is ready now send it for onboarding in batches
                var MessageLength = messageList.Content.ToArray().Length;

                if (messageList.Content.ToArray().Length > 0)
                {
                    double tmpTotalBatches = (double)AgentsEventsCache.AgentEvents.Count / (double)MessageBatchSize;
                    TotalBatches = (int)Math.Ceiling(tmpTotalBatches);

                    if (TotalBatches > 0)
                    {
                        do
                        {
                            RTOnboardingHelper.OnboardBatch(messageList, BatchNo, TotalBatches, "");
                            BatchNo++;
                        } while (BatchNo <= TotalBatches);
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception occurred in: " + ex.StackTrace);
            }

        }


        /// <summary>
        /// Update the Agent state as After call work if the agent has ended the call
        /// </summary>
        /// <param name="agentEvent"></param>
        /// <returns></returns>
        private string checkAgentState(DTO.AgentEvent.DTO_AgentEvent agentEvent)
        {
            try
            {
                //If the event type = statechange  and contact.state = ended and 
                //agentstatus.name = available then agent status = After call Work

                if (agentEvent.CurrentAgentSnapshot.Contacts != null)
                {
                    Log.Debug("Agent placed a call" + agentEvent.CurrentAgentSnapshot.Contacts);

                    if (!string.IsNullOrEmpty(agentEvent.CurrentAgentSnapshot.Contacts[0].State.ToLower()))
                    {
                        //Check if call state is ENDED, If yes then set AgentState as Available
                        if (agentEvent.CurrentAgentSnapshot.Contacts[0].State.ToLower() == "ended")
                        {
                            agentEvent.CurrentAgentSnapshot.AgentStatus.Name = Constant.AFTER_CALL_WORK;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error("Exception occured in: " + MethodBase.GetCurrentMethod().Name);
                Log.Error("Exception details: " + ex.StackTrace);
            }

            return "Available";
        }
        #endregion


    }

}
