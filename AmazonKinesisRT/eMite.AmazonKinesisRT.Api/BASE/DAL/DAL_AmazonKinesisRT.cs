﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bcl = eMite.Framework.Bcl;

namespace eMite.AmazonKinesisRT.Api.BASE.DAL
{
    public abstract class DAL_AmazonKinesisRT : bcl.Base.BM.BM_NoCrudBase, IDisposable
    {
        public abstract bcl.DTO.DTO_Result Start();

        #region "Virtual Methods"

        public virtual void Dispose()
        {

        }

        #endregion
    }
}
