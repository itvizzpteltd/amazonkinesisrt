﻿using System;
using bcl = eMite.Framework.Bcl;

namespace eMite.AmazonKinesisRT.Api.BASE.BM
{
    public abstract class BM_AmazonKinesisRTBase : bcl.Base.BM.BM_NoCrudBase, IDisposable
    {
        #region "Properties"
        public DAC.DAC_AmazonKinesisRT Dac { get; set; }
        #endregion

        #region "Abstract Methods"
        public abstract bcl.DTO.DTO_Result Start();
        #endregion

        #region "Virtual Methods"

        public virtual void Dispose()
        {

        }
        #endregion
    }
}
