﻿using System;
using bcl = eMite.Framework.Bcl;


namespace eMite.AmazonKinesisRT.Api.BASE.DAC
{
    public abstract class DAC_AmazonKinesisRT : bcl.Base.BM.BM_NoCrudBase, IDisposable
    {
        #region "Properties"
        public DAL.DAL_AmazonKinesisRT Dal { get; set; }


        #endregion

        #region "Abstract Methods"
        public abstract bcl.DTO.DTO_Result Start();
        #endregion

        #region "Virtual Methods"

        public virtual void Dispose()
        {

        }

        #endregion
    }
}
