﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bcl = eMite.Framework.Bcl;

namespace eMite.AmazonKinesisRT.Api.DAC
{
    public class DAC_AmazonKinesisRT : BASE.DAC.DAC_AmazonKinesisRT
    {
        #region "Properties"

        #endregion

        #region "Constructors"
        public DAC_AmazonKinesisRT(DTO.DTO_Config config)
        {
            Dal = new DAL.DAL_AmazonKinesis(config);
        }
        
        #endregion

        #region "Overrides"
        public override bcl.DTO.DTO_Result Start()
        {
            return Dal.Start();
        }

        public override void Dispose()
        {
            Dal.Dispose();
        }

        #endregion

    }
}
