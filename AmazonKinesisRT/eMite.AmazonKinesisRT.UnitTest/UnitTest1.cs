﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using tran = eMite.AmazonKinesisRT.Adapter;
using addinview = eMite.Framework.Bcl.Adapters.AddInView;

namespace eMite.AmazonKinesisRT.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ExtractorTest()
        {

            //initialize the extractor class
            tran.BM.BM_Plugin bmPlugin = new Adapter.BM.BM_Plugin();

            //gets a sample configuration that the developer can change for testing.
            addinview.DTO.DTO_HostToAddIn HostToAddInData = bmPlugin.GetSampleConfig();

            //set instance name. Can be any text for testing
            HostToAddInData.Config.CFGInstances[0].UniqueName = "AWSConnectRestartlogic";

            //set the storage type to text file for testing purposes            
            HostToAddInData.Config.SetConfig("StorageType", "TextFile");

            //initialize the adapter before running.
            bmPlugin.InitializeAdapterData(addinview.BM.BM_Helpers.SerializeHostToAddInData(HostToAddInData));

            //run the adapter logic and get serialized results.
            string results = bmPlugin.Process();


        }

        #region "Helpers"

        [TestInitialize()]
        public void Initialize()
        {
            System.Configuration.ConfigurationManager.GetSection("dummy");

        }

        #endregion
    }
}
